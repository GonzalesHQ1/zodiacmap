var currentYear = new Date().getFullYear();

var ram = {
    name: 'Ram',
    startDate: new Date(currentYear, 2, 21),
    endDate: new Date(currentYear, 3, 20),
    coords: {

    }
}
var stier = {
    name: 'Stier',
    startDate: new Date(currentYear, 3, 21),
    endDate: new Date(currentYear, 4, 20),
    coords: {

    }
}
var tweelingen = {
    name: 'Tweelingen',
    startDate: new Date(currentYear, 4, 21),
    endDate: new Date(currentYear, 5, 20),
    coords: {

    }
}
var kreeft = {
    name: 'Kreeft',
    startDate: new Date(currentYear, 5, 21),
    endDate: new Date(currentYear, 6, 22),
    coords: {

    }
}
var leeuw = {
    name: 'Leeuw',
    startDate: new Date(currentYear, 6, 23),
    endDate: new Date(currentYear, 7, 22),
    coords: {

    }
}
var Maagd = {
    name: 'Maagd',
    startDate: new Date(currentYear, 7, 23),
    endDate: new Date(currentYear, 8, 22),
    coords: {

    }
}
var Weegschaal = {
    name: 'Weegschaal',
    startDate: new Date(currentYear, 8, 23),
    endDate: new Date(currentYear, 9, 22),
    coords: {

    }
}
var Schorpioen = {
    name: 'Schorpioen',
    startDate: new Date(currentYear, 9, 23),
    endDate: new Date(currentYear, 10, 22),
    coords: {

    }
}
var Boogschutter = {
    name: 'Boogschutter',
    startDate: new Date(currentYear, 10, 23),
    endDate: new Date(currentYear, 11, 21),
    coords: {

    }
}
var Steenbok = {
    name: 'Steenbok',
    startDate: new Date(currentYear, 11, 22),
    endDate: new Date(currentYear, 0, 20),
    coords: {

    }
}
var Waterman = {
    name: 'Waterman',
    startDate: new Date(currentYear, 0, 21),
    endDate: new Date(currentYear, 1, 20),
    coords: {

    }
}
var Vissen = {
    name: 'Vissen',
    startDate: new Date(currentYear, 1, 21),
    endDate: new Date(currentYear, 2, 20),
    coords: {

    }
}

var sterrenbeelden = {
    ram,
    stier,
    tweelingen,
    kreeft,
    leeuw,
    Maagd,
    Weegschaal,
    Schorpioen,
    Boogschutter,
    Steenbok,
    Waterman,
    Vissen
}

var sterrenbeeldenArr = [
    ram,
    stier,
    tweelingen,
    kreeft,
    leeuw,
    Maagd,
    Weegschaal,
    Schorpioen,
    Boogschutter,
    Steenbok,
    Waterman,
    Vissen
]