/**
 * Vars
 */

var today = new Date();
var map = $('#nightsky');
var correctZodiac;

console.log('Rik, geef mijn pantoffels terug! xoxoxo iz iz');

/**
 * Utils
 */

function getActiveZodiac() {

    var possibleMatches = [];

    for (let index = 0; index < sterrenbeeldenArr.length; index++) {

        var lastZodiac = sterrenbeeldenArr[sterrenbeeldenArr.length - 1]
        var previousZodiac = index === 0 ? lastZodiac : sterrenbeeldenArr[index - 1];
        var selectedZodiac = sterrenbeeldenArr[index];

        // console.log('checking zodiac  - ' + selectedZodiac.name);

        // Check if between start and end date
        if (moment(today).isBetween(selectedZodiac.startDate, selectedZodiac.endDate)) {
            // console.log('found matching zodiac sign ::' + selectedZodiac.name);
            possibleMatches.push(selectedZodiac);
        }
        // Check if between previous enddate and current startdate to handle edge case
        if (moment(today).isBetween(previousZodiac.endDate, selectedZodiac.startDate)) {
            // console.log('found matching zodias sign ::' + previousZodiac.name);
            possibleMatches.push(previousZodiac);
        }
    }

    // console.log('all matching zodiacs')
    // console.log(possibleMatches);

    return possibleMatches[0];
}

function onClickConstellation(name) {
    // console.log('zodiac clicked::' + name);
    if (name === correctZodiac.name) {
        window.location.replace('www.sterrenklub.be/blkxchng');
    } else {
        // console.log('not this one');
    }

}


/**
 * 
 * Init
 */

function setupClickEvent() {
    $("area").on("click", function (e) {
        e.preventDefault();

        var name = $(this).attr('alt');
        // console.log(name);

        onClickConstellation(name);
    });
}


$(document).ready(function () {
    // console.log("ready!");

    correctZodiac = getActiveZodiac();
    setupClickEvent();
});